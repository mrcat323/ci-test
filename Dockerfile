FROM composer:latest as vendor

WORKDIR /app

COPY composer.json composer.json
COPY composer.lock composer.lock

RUN composer install \
    --no-interaction \
    --no-plugins \
    --no-scripts \
    --no-dev \
    --optimize-autoloader \
    --prefer-dist

COPY . .

RUN composer dumpautoload --optimize


FROM php:8.1-fpm AS base

# Install PHP dependencies
RUN apt-get update -y && apt-get install -y curl \
        libfreetype6-dev \
        libpq-dev \
        libzip-dev; \
        apt purge -y --auto-remove && \
        apt clean && \
        rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-install -j$(nproc) pgsql pdo_pgsql zip;
from base AS final

COPY --from=vendor /app /var/www/html
RUN id -u www-data | xargs -I{} chown -R {}:{} /var/www/html/storage/