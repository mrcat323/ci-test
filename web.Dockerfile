FROM nginx:1.25.4-alpine

ADD configuration.conf /etc/nginx/conf.d/default.conf
ADD . /var/www/html